export class LoginUser {
  id: number;
  apiKeys: {
    accessKey: string;
    secretKey: string;
  }
}
