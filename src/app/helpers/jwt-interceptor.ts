import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UrlUtility } from './url-utility';
import { UrlObject } from '@app/models';
import { AuthenticationService } from '@app/services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
    private authenticationService: AuthenticationService,
    private urlUtility: UrlUtility
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    let CurrentUser = this.authenticationService.CurrentUserValue;
    if (CurrentUser && CurrentUser.apiKeys) {
      request = request.clone({
        setHeaders: { }
      });
    }

    return next.handle(request);
  }
}
