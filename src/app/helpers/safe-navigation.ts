export const safeNavigate = (obj) => {
  if (!obj)
    return {};

  return new Proxy(obj, {
    get: function(target, name) {
      const result = target[name];
      if (!!result) {
        return (result instanceof Object) ? safeNavigate(result) : result;
      }
      return safeNavigate({});
    }
  });
}

