import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { LOCAL_STORAGE, InMemoryStorageService } from 'ngx-webstorage-service';

import { AuthenticationService } from '@app/services';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    @Inject(LOCAL_STORAGE) private ngxInMemoryStorage: InMemoryStorageService,
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    const CurrentUser = this.authenticationService.CurrentUserValue;
    if (CurrentUser) {
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.ngxInMemoryStorage.set('returnUrl', state.url || '/');
    this.router.navigate(['/login'], { replaceUrl: true });
    return false;
  }
}
