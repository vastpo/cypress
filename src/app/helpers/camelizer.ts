import { SETTINGS } from '@config/settings';
import * as camelcaseKeys from 'camelcase-keys';

const _isDeep: boolean = true;
const _exclude: Array<string> = SETTINGS.availableLang;

export const camelize = (input, deep: boolean = _isDeep, exclude: Array<string> = _exclude) => {
  return camelcaseKeys(input, { deep: deep, exclude: exclude });
}
