/* https://davidsekar.com/angular/parse-url-in-angular */

import { Injectable } from '@angular/core';
import * as urlParse from 'url-parse';

import { UrlObject } from '@app/models';

@Injectable()
export class UrlUtility {
  public parse(url: string, parseQuery=true): UrlObject {
    return urlParse(url, parseQuery);
  }
}
