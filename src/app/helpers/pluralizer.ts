import { Injectable } from '@angular/core';
import * as pluralize from 'pluralize';

/**
 * Ref: https://www.npmjs.com/package/pluralize
 *
 *   pluralize.addPluralRule(rule, replacement)
 *   pluralize.addSingularRule(rule, replacement)
 *   pluralize.addUncountableRule(word)
 *   pluralize.addIrregularRule(single, plural)
 */

export { pluralize };
