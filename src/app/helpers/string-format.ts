import * as strFormat from 'string-format';

/**
 * Ref: https://www.npmjs.com/package/string-format
 * Please define your initiation here:
 */

export { strFormat };
