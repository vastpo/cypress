import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Select2OptionData } from 'ng2-select2';
import { NgxSpinnerService } from 'ngx-spinner';
import { LOCAL_STORAGE, WebStorageService } from 'ngx-webstorage-service';

import { MomentService, RestService } from '@app/services';
import { SETTINGS } from '@config/settings';

import * as _ from 'lodash';

@Component({
  selector: 'app-post-creation',
  templateUrl: './creation.component.html.pug',
  styleUrls: ['./creation.component.scss']
})
export class CreationComponent implements OnInit {
  private _: any = _;
  private _settings: any = SETTINGS;

  private _exampleData: Array<Select2OptionData>;

  private _form = this.formBuilder.group({
    title: this.formBuilder.group(_.reduce(SETTINGS.availableLang, (obj, lang) => { obj[lang] = ''; return obj; }, {})),
    note: this.formBuilder.group(_.reduce(SETTINGS.availableLang, (obj, lang) => { obj[lang] = ''; return obj; }, {}))
  });

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this._exampleData = [
      {
        id: 'basic1',
        text: 'Basic 1'
      },
      {
        id: 'basic2',
        disabled: true,
        text: 'Basic 2'
      },
      {
        id: 'basic3',
        text: 'Basic 3'
      },
      {
        id: 'basic4',
        text: 'Basic 4'
      }
    ];
  }

  private unifyLangAttr(formGroup: FormGroup, sourceLang: any): void {
    _.forEach(formGroup.controls, (ctrl, lang) => ctrl.setValue(formGroup.value[sourceLang]));
  }
}
