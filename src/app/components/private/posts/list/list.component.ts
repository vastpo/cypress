import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { LOCAL_STORAGE, WebStorageService } from 'ngx-webstorage-service';

import { MomentService, RestService } from '@app/services';
import { SETTINGS } from '@config/settings';

@Component({
  selector: 'app-post',
  templateUrl: './list.component.html.pug',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  private _currentPartner = JSON.parse(this.ngxLocalStorage.get('CurrentPartner') || null);
  private _currentUser = JSON.parse(this.ngxLocalStorage.get('CurrentUser') || null);
  private _schema: any = {};
  private _layout: any = {};

  // private _pageChangeFlag: boolean = false;
  private _currentPage: number;
  private _posts: any = {};
  private _state: String = SETTINGS.defaultModelStateAll;

  private _settings: any = SETTINGS;

  private goToPage(e: any): void {
    this._currentPage = this._posts.page = Math.min(Math.max(1, parseInt(e.target.value)), this._posts.total_pages);
    this.fetchList(this._currentPage, this._state);
    // this.router.navigate([], { relativeTo: this.route, queryParams: { page: this._currentPage }, queryParamsHandling: 'merge' });
  }

  private pageChanged(e: any): void {
    this.fetchList(e.page, this._state);
  }

  private stateChanged(e: any): void {
    this.fetchList(this._currentPage = this._posts.page = 1, this._state = e.state);
  }

  private fetchList(page: number, state: String): void {
    window.scroll(0, 0);
    // if (this._pageChangeFlag == true) {
    //   this._pageChangeFlag = false;
    //   return;
    // }
    this.spinner.show();
    setTimeout(() => {
      this._currentPage = page;

      this.restService.get('partner_admin/posts/index', { qs: { managed_by_id: this._currentPartner.partner_id, state: state, order: 'id:desc', pagination: `page:${page}` } }).then((data) => {
        this.spinner.hide();
        this._posts = data;
      });
      // this.router.navigate([], { relativeTo: this.route, queryParams: { page: page }, queryParamsHandling: 'merge' });
      // this._pageChangeFlag = (page != 1);
    }, 500);
  }

  constructor(
    private moment: MomentService,
    @Inject(LOCAL_STORAGE) private ngxLocalStorage: WebStorageService,
    private restService: RestService,
    private route: ActivatedRoute,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    this._schema = {
      "type": "object",
      "properties": {
        "first_name": { "type": "string" },
        "last_name": { "type": "string" },
        "address": {
          "type": "object",
          "properties": {
            "street_1": { "type": "string" },
            "street_2": { "type": "string" },
            "city": { "type": "string" },
            "state": {
              "type": "string",
              "enum": [ "AL", "AK", "AS", "AZ", "AR", "CA", "CO", "CT", "DE",
                "DC", "FM", "FL", "GA", "GU", "HI", "ID", "IL", "IN", "IA",
                "KS", "KY", "LA", "ME", "MH", "MD", "MA", "MI", "MN", "MS",
                "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND",
                "MP", "OH", "OK", "OR", "PW", "PA", "PR", "RI", "SC", "SD",
                "TN", "TX", "UT", "VT", "VI", "VA", "WA", "WV", "WI", "WY" ]
            },
            "zip_code": { "type": "string" }
          }
        },
        "birthday": { "type": "string" },
        "notes": { "type": "string" },
        "phone_numbers": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "type": { "type": "string", "enum": [ "cell", "home", "work" ] },
              "number": { "type": "string" }
            },
            "required": [ "type", "number" ]
          }
        }
      },
      "required": [ "last_name" ]
    };
    this._layout = [
      { "type": "flex", "flex-flow": "row wrap", "items": [ "first_name", "last_name" ] },
      { "key": "address.street_1", "title": "Address", "placeholder": "Street" },
      { "key": "address.street_2", "notitle": true },
      { "type": "div",
        "display": "flex",
        "flex-direction": "row",
        "items": [
          { "key": "address.city", "flex": "3 3 150px",
            "notitle": true, "placeholder": "City"
          },
          { "key": "address.state", "flex": "1 1 50px",
            "notitle": true, "placeholder": "State"
          },
          { "key": "address.zip_code", "flex": "2 2 100px",
            "notitle": true, "placeholder": "Zip Code"
          }
        ]
      },
      { "key": "birthday", "type": "date" },
      { "key": "phone_numbers",
        "type": "array",
        "listItems": 3,
        "items": [ {
          "type": "div",
          "displayFlex": true,
          "flex-direction": "row",
          "items": [
            { "key": "phone_numbers[].type", "flex": "1 1 50px",
              "notitle": true, "placeholder": "Type"
            },
            { "key": "phone_numbers[].number", "flex": "4 4 200px",
              "notitle": true, "placeholder": "Phone Number"
            }
          ]
        } ]
      },
      { "type": "section",
        "title": "Notes",
        "expandable": true,
        "expanded": false,
        "items": [ { "key": "notes", "type": "textarea", "notitle": true } ]
      }
    ];
    // "data": {
    //   "first_name": "Jane",
    //   "last_name": "Doe",
    //   "address": {
    //     "street_1": "123 Main St.",
    //     "city": "Las Vegas",
    //     "state": "NV",
    //     "zip_code": "89123"
    //   },
    //   "birthday": "1999-09-09",
    //   "phone_numbers": [
    //     { "type": "cell", "number": "702-123-4567" },
    //     { "type": "work", "number": "702-987-6543" }
    //   ],
    //   "notes": "(This is an example of an uninteresting note.)"
    // }
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      // if (!params.page)
      //   return this.router.navigate([], { relativeTo: this.route, queryParams: { page: 1 }, queryParamsHandling: 'merge' });

      let page = parseInt(params.page || 1);
      this._currentPage = page;
      this.fetchList(page, this._state);
    });
  }

}
