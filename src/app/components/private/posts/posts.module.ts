import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Select2Module } from 'ng2-select2';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { TranslateModule } from '@ngx-translate/core';

// Components Routing
import { PostsRoutingModule } from './posts-routing.module';

// Import shared components
import { SharedModule } from '@app/components/shared/shared.module';

// Import child components
import { CreationComponent } from './creation/creation.component';
import { ListComponent } from './list/list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PostsRoutingModule,
    ReactiveFormsModule,
    Select2Module,
    SharedModule,
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn btn-secondary'
    }),
    TranslateModule,
  ],
  declarations: [
    CreationComponent,
    ListComponent,
  ]
})
export class PostsModule {
}
