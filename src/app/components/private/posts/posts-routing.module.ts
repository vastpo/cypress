import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import child components
import { CreationComponent } from './creation/creation.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Posts'
    },
    children: [
      {
        path: '',
        component: ListComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'new',
        component: CreationComponent,
        data: {
          title: 'New'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule {}
