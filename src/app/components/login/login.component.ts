import { AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { LOCAL_STORAGE, InMemoryStorageService, WebStorageService } from 'ngx-webstorage-service';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '@app/services';
import { UrlUtility } from '@app/helpers';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html.pug'
})
export class LoginComponent implements AfterViewInit, OnInit {
  @ViewChild('deleteSwal') private deleteSwal: SwalComponent;
  loginForm: FormGroup;

  constructor(
    @Inject(LOCAL_STORAGE) private ngxInMemoryStorage: InMemoryStorageService,
    @Inject(LOCAL_STORAGE) private ngxLocalStorage: WebStorageService,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private urlUtil: UrlUtility
  ) {
    this.loginForm = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.authenticationService.logout();
  }

  ngAfterViewInit() {
  }

  login() {
    let credential = this.loginForm.value;
    this.authenticationService.login(credential.login, credential.password).subscribe(status => {
      let returnUrl = this.urlUtil.parse(this.ngxInMemoryStorage.get('returnUrl'))
      if (status == 'ok')
        // this.router.navigate([returnUrl.pathname], { queryParams: returnUrl.query, replaceUrl: true });
        this.router.navigate(['/'], { replaceUrl: true });
      else
        this.deleteSwal.show();
    });
  }

  logout() {
    this.authenticationService.logout();
  }
}
