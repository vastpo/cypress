import { Component } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'spinner',
  templateUrl: 'spinner.component.html.pug'
})
export class SpinnerComponent {
  constructor(private spinner: NgxSpinnerService) {
  }
}
