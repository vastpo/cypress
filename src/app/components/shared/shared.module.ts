import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TranslateModule } from '@ngx-translate/core';

// Import shared components
import { PaginatorComponent } from './paginator/paginator.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { StateNavigatorComponent } from './state-navigator/state-navigator.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    PaginationModule.forRoot(),
    TranslateModule,
  ],
  declarations: [
    PaginatorComponent,
    SpinnerComponent,
    StateNavigatorComponent,
  ],
  exports: [
    PaginatorComponent,
    SpinnerComponent,
    StateNavigatorComponent,
  ]
})

export class SharedModule { }
