import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'paginator',
  templateUrl: './paginator.component.html.pug',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent {
  @Input() model: any;
  @Output() pageChanged = new EventEmitter();
  @Output() pageNavigated = new EventEmitter();

  constructor() {
  }

  pageChange($event) {
    this.pageChanged.emit($event);
  }

  pageNavigate($event) {
    this.pageNavigated.emit($event);
  }

}
