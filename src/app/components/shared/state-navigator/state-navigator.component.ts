import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

import { RestService } from '@app/services';
import { SETTINGS } from '@config/settings';
import * as _ from 'lodash';

@Component({
  selector: 'state-navigator',
  templateUrl: './state-navigator.component.html.pug',
  styleUrls: ['./state-navigator.component.scss']
})
export class StateNavigatorComponent implements OnInit {
  @Input() model: String;
  @Input() state: String;
  @Output() stateChanged: EventEmitter<any> = new EventEmitter();

  public static readonly ROW_SIZE = 12;
  states: any = [];

  constructor(private restService: RestService) {
  }

  ngOnInit(): void  {
    this.restService.get('general/states', { qs: { model: this.model } }).then((data) => {
      let states = _.union([ SETTINGS.defaultModelStateAll ], data);
      for (let row: number = 0; row < Math.ceil(states.length/StateNavigatorComponent.ROW_SIZE); row++) {
        this.states[row] = [];
        for (let col: number = 0; col < StateNavigatorComponent.ROW_SIZE; col++) {
          let state = states[row*StateNavigatorComponent.ROW_SIZE+col];
          if (state) this.states[row][col] = state;
        }
      }
    });
  }

  stateChange($event): void {
    this.stateChanged.emit($event);
  }
}
