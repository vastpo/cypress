import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE, WebStorageService } from 'ngx-webstorage-service';

import * as _ from 'lodash';
import * as jwt from 'angular2-jwt-simple';

import { safeNavigate } from '@app/helpers/safe-navigation';
import { strFormat } from '@app/helpers/string-format';
import { LoginUser, RequestParams } from '@app/models';
import { ApiDefinitions } from '@config/api-definitions';
import { ENV } from '@env';

@Injectable({ providedIn: 'root' })
export class RestService {
  private apiPrefix: string;
  private apiDefinitions: any = ApiDefinitions;

  constructor(
    private http: HttpClient,
    @Inject(LOCAL_STORAGE) private ngxLocalStorage: WebStorageService,
    private toastr: ToastrService,
    private translate: TranslateService
  ) {
    this.apiPrefix = `${ENV.api.baseUrl}/api`;
  }

  public get(route: string, params: any = {}, options: any = {}): any {
    options = _.defaults(options, { promise: true });
    const _params = this._formatParams(params);
    try {
      const definition = this._seekDefinition(route, this.apiDefinitions.get);
      if (_.isEmpty(definition))
        throw new Error(`ERROR: API Definition for GET ${route} not found!`);
      const headers = this._buildHeaders(definition, _params);
      const url = `${this.apiPrefix}/${strFormat(definition.url, _params.subst)}`;
      if (options.promise)
        return new Promise((resolve, reject) => {
          this.http.get<any>(url, { headers: headers, observe: 'response', params: _params.qs }).subscribe((response) => {
            if (response.status == 200)
              resolve(response.body);
            else
              reject(response.body);
          }, (response) => {
            this._displayErrorToast(`error.message.${response.error.status}`)
          });
        });
      else
        return this.http.get<any>(url, { headers: headers, observe: 'response', params: _params.qs });
    }
    catch (e) {
      console.error(e.message);
    }
  }

  public post(route: string, params: any = {}, options: any = {}): any {
    options = _.defaults(options, { promise: true });
    const _params = this._formatParams(params);
    try {
      const definition = this._seekDefinition(route, this.apiDefinitions.post);
      if (_.isEmpty(definition))
        throw new Error(`ERROR: API Definition for POST ${route} not found!`);
      const headers = this._buildHeaders(definition, _params);
      const url = `${this.apiPrefix}/${strFormat(definition.url, _params.subst)}`;
      if (options.promise)
        return new Promise((resolve, reject) => {
          this.http.post<any>(url, _params.body, { headers: headers, observe: 'response', params: _params.qs }).subscribe((response) => {
            if (response.status == 200)
              resolve(response.body);
            else
              reject(response.body);
          }, (response) => {
            this._displayErrorToast(`error.message.${response.error.status}`)
          });
        });
      else
        return this.http.post<any>(url, _params.body, { headers: headers, observe: 'response', params: _params.qs });
    }
    catch (e) {
      console.error(e.message);
    }
  }

  public put(route: string, params: any = {}, options: any = {}): any {
    options = _.defaults(options, { promise: true });
    const _params = this._formatParams(params);
    try {
      const definition = this._seekDefinition(route, this.apiDefinitions.put);
      if (_.isEmpty(definition))
        throw new Error(`ERROR: API Definition for PUT ${route} not found!`);
      const headers = this._buildHeaders(definition, _params);
      const url = `${this.apiPrefix}/${strFormat(definition.url, _params.subst)}`;
      if (options.promise)
        return new Promise((resolve, reject) => {
          this.http.put<any>(url, _params.body, { headers: headers, observe: 'response', params: _params.qs }).subscribe((response) => {
            if (response.status == 200)
              resolve(response.body);
            else
              reject(response.body);
          }, (response) => {
            this._displayErrorToast(`error.message.${response.error.status}`)
          });
        });
      else
        return this.http.put<any>(url, _params.body, { headers: headers, observe: 'response', params: _params.qs });
    }
    catch (e) {
      console.error(e.message);
    }
  }

  public delete(route: string, params: any = {}, options: any = {}): any {
    options = _.defaults(options, { promise: true });
    const _params = this._formatParams(params);
    try {
      const definition = this._seekDefinition(route, this.apiDefinitions.delete);
      if (_.isEmpty(definition))
        throw new Error(`ERROR: API Definition for DELETE ${route} not found!`);
      const headers = this._buildHeaders(definition, _params);
      const url = `${this.apiPrefix}/${strFormat(definition.url, _params.subst)}`;
      if (options.promise)
        return new Promise((resolve, reject) => {
          this.http.delete<any>(url, { headers: headers, observe: 'response', params: _params.qs }).subscribe((response) => {
            if (response.status == 200)
              resolve(response.body);
            else
              reject(response.body);
          }, (response) => {
            this._displayErrorToast(`error.message.${response.error.status}`)
          });
        });
      else
        return this.http.delete<any>(url, { headers: headers, observe: 'response', params: _params.qs });
    }
    catch (e) {
      console.error(e.message);
    }
  }

  private _buildHeaders(apiDefinition: any, params: any) {
    let headers = { 'Accept': `application/vnd.acme-${apiDefinition.version}+json` }
    if (apiDefinition.isPrivate) {
      const TIME_NOW = Math.floor(Date.now() / 1000);
      const { id, type, apiKey } = JSON.parse(this.ngxLocalStorage.get('CurrentUser'));
      const payload = {
        id: id,
        type: type,
        time: TIME_NOW,
        exp: TIME_NOW + ENV.api.jwtLife,
        params: { ...params.subst, ...params.qs, ...params.body }
      }
      headers = _.extend({
        'Access-Key': apiKey.accessKey,
        'Signature': jwt.encode(payload, apiKey.secretKey, 'HS512')
      }, headers);
    }
    return headers;
  }

  private _seekDefinition(route: string, root: any) {
    const [ head, ...tail ] = _.split(route, '/')
    if (!_.isEmpty(tail))
      return this._seekDefinition(_.join(tail, '/'), root[head]);
    else
      return root[head];
  }

  private _displayErrorToast(message: string) {
    this.translate.get(['error.caption.general', message]).subscribe((res: any) => {
      this.toastr.error(res[message], res['error.caption.general'], { closeButton: true, positionClass: 'toast-top-full-width' })
    });
  }

  private _formatParams(params: any) {
    params.subst = params.subst || {};
    params.qs = params.qs || {};
    params.body = params.body || {}
    return <RequestParams>params;
  }
}
