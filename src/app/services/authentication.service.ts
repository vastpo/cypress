import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LOCAL_STORAGE, WebStorageService } from 'ngx-webstorage-service';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { camelize } from '@app/helpers/camelizer';
import { RestService } from '@app/services/rest.service';
import { LoginUser } from '@app/models';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private CurrentUserSubject: BehaviorSubject<LoginUser>;
  public CurrentUser: Observable<LoginUser>;

  constructor(
    private http: HttpClient,
    private restService: RestService,
    @Inject(LOCAL_STORAGE) private ngxLocalStorage: WebStorageService
  ) {
    this.CurrentUserSubject = new BehaviorSubject<LoginUser>(JSON.parse(ngxLocalStorage.get('CurrentUser') || null));
    this.CurrentUser = this.CurrentUserSubject.asObservable();
  }

  public get CurrentUserValue(): LoginUser {
    return this.CurrentUserSubject.value;
  }

  login(login: string, password: string) {
    return this.restService.post('partner_admin/signin', { body: { login, password }}, { promise: false }).pipe(map((response: any) => {
      // login successful if there's a jwt token in the response
      if (response.body.status == 'ok') {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        this.ngxLocalStorage.set('CurrentUser', JSON.stringify(camelize(response.body)));
        this.CurrentUserSubject.next(camelize(response.body));
      }

      return response.body.status;
    }));
  }

  logout() {
    // remove user from local storage to log user out
    this.ngxLocalStorage.remove('CurrentUser');
    this.ngxLocalStorage.remove('CurrentLanguage');
    this.ngxLocalStorage.remove('CurrentPartner');
    this.CurrentUserSubject.next(null);
  }
}
