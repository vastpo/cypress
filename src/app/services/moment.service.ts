import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, WebStorageService } from 'ngx-webstorage-service';

import * as moment from 'moment-timezone';

@Injectable({ providedIn: 'root' })
export class MomentService {
  public static readonly CONCISE = 'DD MMM YYYY HH:mm z';
  public static readonly LONG    = 'DD MMMM YYYY HH:mm:ss z';

  private _timezone: string;

  constructor(@Inject(LOCAL_STORAGE) private ngxLocalStorage: WebStorageService) {
    let _currentUser = JSON.parse(this.ngxLocalStorage.get('CurrentUser') || null);
    this._timezone = _currentUser.timezone;
  }

  public format(datetime: string, format: string = MomentService.CONCISE, timezone: string = this._timezone) {
    return moment(datetime).tz(timezone).format(format);
  }
}
