import { Component, Inject, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { LOCAL_STORAGE, SESSION_STORAGE, WebStorageService } from 'ngx-webstorage-service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  styleUrls: [ 'app.component.scss' ],
  templateUrl: 'app.component.html.pug'
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    public translate: TranslateService,
    @Inject(LOCAL_STORAGE) private ngxLocalStorage: WebStorageService,
    @Inject(SESSION_STORAGE) private ngxSessionStorage: WebStorageService
  ) {
    translate.setDefaultLang(this.ngxLocalStorage.get('DefaultLanguage'));
    translate.use(this.ngxLocalStorage.get('CurrentLanguage'));
  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
