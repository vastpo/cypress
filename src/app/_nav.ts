export const navItems = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
  },
  {
    name: 'Posts',
    url: '/posts',
    icon: 'fa fa-newspaper-o',
  },
];
