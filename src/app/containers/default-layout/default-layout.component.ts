import { Component, Inject, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { navItems } from '@app/_nav';
import { TranslateService } from '@ngx-translate/core';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { LOCAL_STORAGE, WebStorageService } from 'ngx-webstorage-service';
import * as _ from 'lodash';
import * as aSync from 'async';

import { camelize, pluralize, safeNavigate, strFormat } from '@app/helpers';
import { RestService } from '@app/services';
import { SETTINGS } from '@config/settings';

@Component({
  selector: 'app-dashboard',
  styleUrls: [ './default-layout.component.scss' ],
  templateUrl: './default-layout.component.html.pug'
})
export class DefaultLayoutComponent implements OnDestroy, OnInit {
  @ViewChild('defaultPartnerRevokedSwal') private defaultPartnerRevokedSwal: SwalComponent;
  @ViewChild('switchPartnerConfirmSwal') private switchPartnerConfirmSwal: SwalComponent;
  @ViewChild('switchPartnerFailureSwal') private switchPartnerFailureSwal: SwalComponent;

  private navItems = navItems;
  private sidebarMinimized = true;
  private changes: MutationObserver;
  private element: HTMLElement = document.body;
  private pluralize: any = pluralize;

  private _currentPartner = (JSON.parse(this.ngxLocalStorage.get('CurrentPartner') || null));
  private _revokedPartner: any;
  private _currentUser = JSON.parse(this.ngxLocalStorage.get('CurrentUser') || null);
  private _gotoPartner: any;
  private _me: any = {};
  private _settings: any = SETTINGS;

  constructor(
    @Inject(LOCAL_STORAGE) private ngxLocalStorage: WebStorageService,
    private restService: RestService,
    private translate: TranslateService
  ) {
    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: [ 'class' ]
    });
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  ngOnInit(): void {
    aSync.waterfall([
      (wCallback) => {
        this.restService.post('partner_admin/show').then((data) => {
          this._me = data;
          wCallback(null, data);
        });
      },
      (_me, wCallback) => {
        const _firstPartnerAccess: any = _.first(_me.managing_partners);

        if (!_.isEmpty(this._currentPartner) && !_.keyBy(_me.managing_partners, 'partner_access_id')[this._currentPartner.partner_access_id]) {
          this._revokedPartner = this._currentPartner;
          this.defaultPartnerRevokedSwal.show();
        }

        if (!_.isEqual(this._currentPartner, _firstPartnerAccess)) {
          this.restService.put('partner_admin/partner_access/modify', { subst: { id: _firstPartnerAccess.partner_access_id }, body: { is_default: true }}).then((data) => {
            this.ngxLocalStorage.set('CurrentPartner', JSON.stringify(_firstPartnerAccess));
            this._currentPartner = _firstPartnerAccess;
            wCallback(null);
          });
        }
      }
    ], (err) => {
      this.initData();
    });
  }

  private initData() {
  }

  private switchPartnerCheck(partner_access_id: number) {
    aSync.waterfall([
      (wCallback) => {
        this.restService.post('partner_admin/show').then((data) => {
          this._me = data;
          wCallback(null, _.keyBy(data.managing_partners, 'partner_access_id'));
        });
      },
      (_managing_partners, wCallback) => {
        this._gotoPartner = _managing_partners[partner_access_id];
        if (!this._gotoPartner)
          return wCallback(this.switchPartnerFailureSwal.show());
        wCallback(this.switchPartnerConfirmSwal.show());
      }
    ]);
  }

  private switchPartnerAction() {
    this.restService.put('partner_admin/partner_access/modify', { subst: { id: this._gotoPartner.partner_access_id }, body: { is_default: true }}).then((data) => {
      location.replace('/');
    });
  }
}
