export const ENV = {
  production: false,

  api: {
    baseUrl: 'http://vpdev.stephycat.com:8080',
    jwtLife: 30  // seconds
  }
};
