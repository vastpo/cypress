const definitions = {
  get: {
    general: {
      states: {
        isPrivate: false,
        url: 'general/state.json',
        version: 'v1'
      }
    },
    partner_admin: {
      posts: {
        index: {
          isPrivate: true,
          url: 'partner_admin/posts/index.json',
          version: 'v1'
        }
      },
    }
  },
  post: {
    partner_admin: {
      show: {
        isPrivate: true,
        url: 'partner_admin/show.json',
        version: 'v1'
      },
      signin: {
        isPrivate: false,
        url: 'partner_admin/signin.json',
        version: 'v1'
      }
    }
  },
  put: {
    partner_admin: {
      partner_access: {
        modify: {
          isPrivate: true,
          url: 'partner_admin/partner_access/modify/{id}.json',
          version: 'v1'
        }
      }
    }
  },
  delete: {
  }
};

export const ApiDefinitions = definitions;
