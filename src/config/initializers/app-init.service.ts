/**
 * Reference:
 * https://www.intertech.com/Blog/angular-4-tutorial-run-code-during-app-initialization
 */

import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LOCAL_STORAGE, SESSION_STORAGE, WebStorageService } from 'ngx-webstorage-service';
import 'rxjs/add/operator/toPromise';

import { SETTINGS } from '@config/settings';

@Injectable()
export class AppInitService {

  constructor(
    private httpClient: HttpClient,
    @Inject(LOCAL_STORAGE) private ngxLocalStorage: WebStorageService
  ) { }

  initializeApp(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.ngxLocalStorage.set('DefaultLanguage', this.ngxLocalStorage.get('DefaultLanguage') || SETTINGS.defaultLanguage);

      if (!this.ngxLocalStorage.get('CurrentLanguage')) {
        this.ngxLocalStorage.set('CurrentLanguage', this.ngxLocalStorage.get('DefaultLanguage'));
      }
      resolve();
      // console.log(`initializeApp:: inside promise`);

      // setTimeout(() => {
      //   console.log(`initializeApp:: inside setTimeout`);
      //   // doing something
      //
      //   resolve();
      // }, 10);
    });
  }

  getSettings(): Promise<any> {
    // console.log(`getSettings:: before http.get call`);

    const promise = this.httpClient.get('http://private-1ad25-initializeng.apiary-mock.com/settings').toPromise().then(settings => {
      SETTINGS.currentLanguage = this.ngxLocalStorage.get('CurrentLanguage');
      return settings;
    });

    return promise;
  }
}

