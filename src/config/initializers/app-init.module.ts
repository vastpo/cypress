/**
 * Reference:
 * https://www.intertech.com/Blog/angular-4-tutorial-run-code-during-app-initialization
 */

import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";

import { AppInitService } from './app-init.service';

export function init_app(appInitService: AppInitService) {
  return () => appInitService.initializeApp();
}

export function get_settings(appInitService: AppInitService) {
  return () => appInitService.getSettings();
}

@NgModule({
  imports: [HttpClientModule],
  providers: [
    AppInitService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppInitService], multi: true },
    { provide: APP_INITIALIZER, useFactory: get_settings, deps: [AppInitService], multi: true }
  ]
})
export class AppInitModule { }
