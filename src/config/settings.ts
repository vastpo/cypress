export const SETTINGS = {
  api: {
    jwtAlgorithm: 'HS512',
  },
  availableLang: [
    'en-US',
    'zh-CN',
    'zh-HK',
    'zh-TW',
  ],
  connectionString: null,
  currentLanguage: null,
  defaultImageUrl: null,
  defaultLanguage: 'en-US',
  defaultModelStateAll: 'all',
  externalId: {
    displayLength: 10
  },
  menu: {
    userMenu: {
      item: {
        maxDisplayChars: 24
      }
    }
  },
  stateClass: {
    active: 'success',
    draft: 'light',
    inactive: 'secondary',
    pending: 'warning',
    published: 'success',
    rejected: 'danger',
    reviewing: 'secondary',
    suspended: 'danger',
    withheld: 'warning'
  }
};
